"use client"
import { News } from "@/app/models/news.model";
import ListNewsPage from "../page";
import { Suspense } from "react";
import { useRouter } from "next/navigation";

async function loadNews(idNews: number) {
  const res = await fetch(`http://localhost:8080/news/${idNews}`);
  const data: News  = await res.json();
  await new Promise((resolve) => setTimeout(resolve, 3000))
  return data;
}

async function Page ({ params }: {params: any}) {
  const router = useRouter();
  const news = await loadNews(params.newsId);

  function regret () {
    router.push('/listNews')
  }

  return (
    <>
      <div className="bg-blue-200 p-10 rounded-md">
        <h3 className="text-xl font-bold mb-4">{ news.id } { news.title }</h3>
        <span><em>  { news.author } { news.place } </em></span>
        <p>{ news.content } </p>
        <div>
          <button className="bg-green-500 px-3 py-2 rounded-md" onClick={regret}>Return</button>
        </div>
      </div>
      <hr />
      <h4>Other news</h4>
      <Suspense fallback={<div>Loading news...</div>}>
        <ListNewsPage/>
      </Suspense>
    </>
  )
}

export default Page
