import CardComponent from "../components/card";
import { News } from "../models/news.model";
import "../ui/news.css"

export async function loadListNews() {
    const res = await fetch('http://localhost:8080/news/search?criteria=co&param=content&order=DESC');
    const data: News[]  = await res.json();
    await new Promise((resolve) => setTimeout(resolve, 3000))
    return data;
}

async function ListNewsPage () {
    const news: News[] = await loadListNews();
    return (
        <div className="grid">
            { news.map((newsItem: News) => 
                <CardComponent key={ newsItem.id } news = { newsItem }/>
            )}
        </div> 
    )
}
export default ListNewsPage;
