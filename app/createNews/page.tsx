"use client"

import { useRouter } from "next/navigation";
import { NewsInput } from "../models/news.inputs";

const CreateNewsPage = () => {

  const route = useRouter();

  const onSubmit = async(event: any) => {
    event.preventDefault()
    const title = event.target.title.value
    const content = event.target.content.value
    const author = event.target.author.value
    const place = event.target.place.value
    const type = event.target.typeSelect.value
    const newsInput: NewsInput = {
      title: title,
      content: content,
      author: author,
      place: place,
      type: type,
      postDate: new Date()
    }
    console.log(newsInput)
    await fetch(`http://localhost:8080/news`, {
      method: 'POST',
      body: JSON.stringify(newsInput),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    route.push('/listNews')
  }

  return (
    <div className="h-screen flex justify-center items-center">
      <form className="bg-blue-200 p-10 w-3/4" 
        onSubmit={onSubmit}
      >
        <label htmlFor="title">Title </label>
        <input className="border border-gray-400 py-2 mb-4 w-full" type="text" id="title" />
        <label htmlFor="author">Author </label>
        <input className="border border-gray-400 py-2 mb-4 w-full" type="text" id="author" />
        <label htmlFor="content">Content </label>
        <textarea rows={3}
          className="border border-gray-400 p-2 mb-4 w-full text-black" id="content"
        ></textarea>
        <label htmlFor="place">Place </label>
        <input className="border border-gray-400 py-2 mb-4 w-full" type="text" id="place" />
        <label htmlFor="typeSelect">Selecciona un tipo: </label>
        <select name="type" id="typeSelect">
          <option value="POLITIC">POLITIC</option>
          <option value="CULTURE">CULTURE</option>
          <option value="SCIENCE">SCIENCE</option>
        </select>
        <button  className="bg-sky-500 px-3 py-2 rounded-md">Aceptar</button>
        <button  className="bg-red-500 px-3 py-2 rounded-md" onClick={()=> {route.push('/listNews')}}>Cancelar</button>
      </form>
    </div>
  )
}
export default CreateNewsPage;
