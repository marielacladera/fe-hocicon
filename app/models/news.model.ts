import { TypeEnum } from "./enums/type.enum";

export interface News {
    id: number;
    title: string;
    postDate: Date;
    author: string;
    place: string;
    content: string;
    type: TypeEnum;
}