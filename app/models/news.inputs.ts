import { TypeEnum } from './enums/type.enum';

export interface NewsInput {
    author: string;
    content: string;
    place: string;
    postDate: Date;
    title: string;
    type: TypeEnum;
}