"use client"
import { News } from "@/app/models/news.model";
import Link from "next/link";
import { useRouter } from "next/navigation";


const CardComponent = ({ news }: { news: News }) => {

  const router = useRouter();

  async function deleteNews() {
    await fetch(`http://localhost:8080/news/${news.id}`, {
      method: 'DELETE',
    });
    router.push('/listNews')
  }

  return (
    <div className=" bg-blue-200 p-10">
      <div>
        <Link href={`/listNews/${news.id}`}>
          <h3 className="text-xl font-bold mb-4">{ news.id } { news.title }</h3>
        </Link>
        <p><em>Content: </em>{ news.content }</p>
        <p><em>Author: </em> { news.author }</p>
        <p><em>Place: </em>{ news.place }</p>
        <p><em>Type: </em>{ news.type }</p>
        <div>
          <button className="bg-sky-500 px-3 py-2 rounded-md"><Link href="../../createnews">Update</Link></button>
          <button className="bg-red-500 px-3 py-2 rounded-md" onClick={deleteNews}>Delete</button>
        </div>
      </div>
    </div>
  )
}

export default CardComponent