import Link from "next/link"
import "../ui/navbar.css"

const NavbarComponent = () => {
  return (
        <nav className="navbar py-5">
          <Link href="/"><h1 className="text-3xl font-bold">El Hocicon</h1></Link>
          <ul>
            <li><Link href="/listNews"> News </Link></li>
            <li><Link href="/search"> Search </Link></li>
            <li><Link href="/createNews"> Create News </Link></li>
          </ul>
        </nav>
  )
}

export default NavbarComponent
