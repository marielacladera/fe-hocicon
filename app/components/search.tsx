"use client"

import { News } from "../models/news.model";
import CardComponent from "./card";


const SearchComponent = () => {

  const onSubmit = async(event: any) => {
    event.preventDefault();
    const body = {
      value: event.target.criteria.value,
      param: event.target.param.value,
      order: event.target.order.value
    };
    return body;
    const res = await fetch(`http://localhost:8080/news/search?criteria=${body.value}&param=${body.param}&order=${body.order}`);
    const data: News[] = await res.json();
  
  }

  return (
    <>
      <div className="h-screen items-center">
        <form className="bg-blue-200 p-2" onSubmit={onSubmit}>
          <label htmlFor="criteria">Value </label>
          <input type="text" id="criteria"/> 
          <label htmlFor="param">Elige un parametro </label>
          <select name="param" id="param">
            <option value="title">Title</option>
            <option value="author">Author</option>
            <option value="place">Place</option>
            <option value="content">Content</option>
          </select>
          <label htmlFor="order">Order </label>
          <select name="order" id="order">
            <option value="ASC">ASC</option>
            <option value="DESC">DESC</option>
          </select>
          <button type="submit" className="bg-blue-500 px-3 py-2 rounded-md">Search</button>
        </form>
      </div>
    </>
  )
}

export default SearchComponent
