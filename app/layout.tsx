import { inter } from "./ui/fonts";
import NavbarComponent from "./components/navbar";
import "./ui/globals.css"
import { Container } from "postcss";

export const metadata = {
  title: "El Hocicon"
}

export default function RootLayout ({ children }: { children: React.ReactNode} ) {
  return (
    <html>
      <body className="{`${inter.className} antialiased`} container mx-auto items-center" >
       <NavbarComponent />
        {children}
      </body>
    </html>
  )
}
